CREATE TABLE `dmkhoa` (

  `MAKH` varchar(6) NOT NULL,

  `TenKhoa` varchar(30) NOT NULL

);


CREATE TABLE `sinhvien` (

  `MaSV` varchar(6) NOT NULL,

  `HoSV` varchar(30) NOT NULL,

  `TenSV` varchar(15) NOT NULL,

  `GioiTinh` char(1) NOT NULL,

  `NgaySinh` datetime NOT NULL,

  `NoiSinh` varchar(50) NOT NULL,

  `DiaChi` varchar(50) NOT NULL,

  `MaKH` varchar(6) NOT NULL,

  `HocBong` int(11) NOT NULL

);


ALTER TABLE `dmkhoa`

  ADD PRIMARY KEY (`MAKH`);


ALTER TABLE `sinhvien`

  ADD PRIMARY KEY (`MaSV`);